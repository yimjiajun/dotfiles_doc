# SALEAE

Effortlessly decode protocols like SPI, I2C, Serial, and many more.
Leverage community created analyzers or build your own low-level or high-level protocol analyzer.

software download: [Logic](https://www.saleae.com/downloads)

- [Note](saleae_note.md)

## Introduction

### Execute Program

Executable program for saleae software in window:

![logic_exe](../img/saleae/logic_exe.jpg)

### Home page

Home page without connecting saleae device

![home_page](../img/saleae/homepage.jpg)

### Device Settings

Connector settings to enable / disable displays connector and measurement frequency.

![device_settings](../img/saleae/device_settings.jpg)

### Analyzers

This sections is customize or selection on connecting interface. Which is able to analyze connecting interface as data.

1. The first icon on right side-bar

    ![analyzers](../img/saleae/analyzers.jpg)

2. Add Filter: select connecting interface

    ![analyzers_add_filter](../img/saleae/analyzers_add_filter.jpg)

3. Data Table: displays analyzed data datails (ex. i2c)

    ![analayzers_data_table](../img/saleae/analyzers_data_table.jpg)

4. Terminal: displays analuzd data / ascii / etc. on display as terminal (ex. serial)

    ![analyzers_terminal](../img/saleae/analyzers_terminal.jpg)

### Measurements

Measurement specfic range of signal details

![measurements](../img/saleae/measurement.jpg)

### Extensions

Extra setup from other users. which able to implements on own project to improve measurement.

![extensions](../img/saleae/extensions.jpg)

# SALEAE - NOTE

How-to-Note on Saleae program

## TOPIC:

1. [Timing Markers and Measurement and Notes](#timing-markers-and-measurement-notes)
    1. [Timing Markers](#timing-markers)
        - [Add Single Markers](#add-single-marker)
        - [Add Markers Pairs](#add-marker-pair)
        - [Note](#timing-markers-note)
    1. [Measurement](#measurement)
    1. [Notes](#notes)

## Timing Markers and Measurement, Notes

Specific range of signal to measurement

![timing_markers_measurement_notes](../img/saleae/timing_markers_measure_notes.jpg)

### Timing Markers

Specific timing on measurement

![Timing Markers](../img/saleae/measurement_timing_markers.jpg)

#### Add Single Marker

![add_single_marker](../img/saleae/measurement_timing_markers_add_single_marker.jpg)

1. Move to specific timing of signal to measurement

    ![add_single_marker_select](../img/saleae/measurement_timing_markers_add_single_marker_select.jpg)

2. Click the primary button of mouse to select measurement timing

    ![add_single_marker_selected](../img/saleae/measurement_timing_markers_add_single_marker_select_0.jpg)

##### Add marker pair

![add_marker_pair](../img/saleae/measurement_timing_markers_add_marker_pair.jpg)

1. Move to begin on specific timing of signal to measurement

    ![add_marker_pair_begin](../img/saleae/measurement_timing_markers_add_marker_pair_select_1_begin.jpg)

2. Move to end on specific timing of signal to measurement

    ![add_marker_pair_end](../img/saleae/measurement_timing_markers_add_marker_pair_select_1_end.jpg)

3. After selected a pair of specific timing

    ![add_marker_pair_done](../img/saleae/measurement_timing_markers_add_marker_pair_select_1_done.jpg)

#### timing markers note

Add some details or bookmarks on specific timing

1. Click the _3 dots_

    ![timing_markers_note](../img/saleae/measurement_timing_markers_edit.jpg)

2. Add comment/details of signal

    ![timing_markers_comments](../img/saleae/measurement_timing_markers_edit_comment.jpg)

### Measurement

![measurement](../img/saleae/measurement.jpg)

1. Select begin of signal to measurement

    ![measurement_begin](../img/saleae/measurement_add_select_1_begin.jpg)

2. Select end of signal to measurement

    ![measurement_end](../img/saleae/measurement_add_select_1_end.jpg)

3. After selected of signal to measurement

    ![measurement_done](../img/saleae/measurement_add_select_1_done.jpg)

### Notes

Sharing details of signal information

![note](../img/saleae/measurement_notes.jpg)

# Sensors

print sensors information (ex. fan speed, temperature ...)

## Topic

[Usage](./sensors_display.md)

## Installation

Debian/Ubuntu

```bash
sudo apt-get install lm-sensors
```

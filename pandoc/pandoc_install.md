# Installation

Ubuntu

```bash
sudo apt-get install pandoc
```

---

## Packages

Install pdf engine packages

> `apt-get` for Ubuntu

```bash
sudo apt-get install texlive-latex-base texlive-latex-extra texlive-xetex
```

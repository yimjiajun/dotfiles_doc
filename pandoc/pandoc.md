# Pandoc

General markup converter

Pandoc is a Haskell library for converting from one markup format to another, and a command-line tool that uses this library.

---

## Topic

1. [Installation](./pandoc_install.md)
2. [Usage](./pandoc_usage.md)

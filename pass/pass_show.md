# pass - Show password

**Show secrete/password from *pass-name*[^p1] [^p2]**

```bash
pass show <pass-name>
```
## Copy to clipboard

**Copy secrete/password to clipboard without display**

```bash
pass show -c <pass-name>
```

---

[^p1]: **DO NOT** setup with username, this will displayed as file name. ex. `nvim`, `chatpgt/nvim`
[^p2]: *pass-name* can being research by <kbd> Tab </kbd>

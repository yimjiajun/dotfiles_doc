# pass

[the standard unix password manager](https://www.passwordstore.org/)

Password management should be simple and follow Unix philosophy. With pass, each password lives inside of a gpg encrypted file whose filename is the title of the website or resource that requires the password. These encrypted files may be organized into meaningful folder hierarchies, copied from computer to computer, and, in general, manipulated using standard command line file management utilities.

## Topic

|    | Sections                                       | sub-sections                                                        |
| -  | -                                              | -                                                                   |
| 1  | [Installation](pass_installation.md)           | [Initialize with gpg key](pass_installation.md#initialize-with-key) |
| 3  | [Create password](pass_create.md)              | [Manual](pass_create.md#manual)                                     |
|    |                                                | [Generate](pass_create.md#generate)                                 |
| 4  | [List password](pass_list.md)                  |                                                                     |
| 5  | [Search contents](pass_search.md)              |                                                                     |
| 6  | [Show password](pass_show.md)                  |                                                                     |
| 7  | [Change password](pass_change.md)              |                                                                     |
| 8  | [Delete password](pass_delete.md)              |                                                                     |
| 9  | [Restore password](pass_restore.md)            |                                                                     |
| 10 | [Add additional information](pass_add_info.md) |                                                                     |

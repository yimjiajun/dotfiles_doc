# pass - List password

**1\. Lists all *pass-name***

```bash
pass
```

**2\. Lists corresponding *pass-name***

```bash
pass find <pass-name>
```

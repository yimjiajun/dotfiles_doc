# pass - Create a new password

- [Manual](#manual)

- [Generate](#generate)

## Manual

**1\. Create new password for *pass-name*[^p1]**

```bash
pass insert <pass-name>
```

## Generate

**1\. Assign a generated password from pass for *pass-name*[^p1]**

```bash
pass generate <pass-name>
```
---

[^p1]: **DO NOT** setup with username, this will displayed as file name. ex. `nvim`, `chatpgt/nvim`

# pass - Change password

**Change secrete/password from *pass-name*[^p1] [^p2]**

```bash
pass edit <pass-name>
```

---

[^p1]: **DO NOT** setup with username, this will displayed as file name. ex. `nvim`, `chatpgt/nvim`
[^p2]: *pass-name* can being research by <kbd> Tab </kbd>

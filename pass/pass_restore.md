# pass - Restore password

**Restore deleted password by git**

```bash
pass git revert HEAD
```

> Other git command is allowable to using in restore

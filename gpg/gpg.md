# GPG

[GnuPG](https://gnupg.org/index.html) is a complete and free implementation of the OpenPGP standard as defined by RFC4880 (also known as PGP).

GnuPG allows you to encrypt and sign your data and communications;

It features a versatile key management system, along with access modules for all kinds of public key directories.

GnuPG, also known as GPG, is a command line tool with features for easy integration with other applications.

## Topic

|   | Sections                                | sub-sections |
| - | -                                       | -            |
| 1 | [Generate Key](gpg_generate_keypair.md) |              |
| 2 | [Lists Key](gpg_list_keys.md)           |              |
| 3 | [Export Key](gpg_export_key.md)         |              |
| 4 | [Import Key](gpg_import_key.md)         |              |

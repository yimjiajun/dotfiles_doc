# GPG - Lists keypairs

Lists public keys

```bash
gpg -k
```

Lists secret keys (private key)

```bash
gpg -K
```

# Dotfiles

- [Introductions](Home.md)

- [Summary](SUMMARY.md)

- [Bluetooth](bluetooth/bluetooth.md)

  - [Interactive](bluetooth/bluetooth_interactive.md)
  - [Controller](<>)
    - [Controller status](bluetooth/bluetooth_controller_status.md)
    - [Controller multiple](bluetooth/bluetooth_controller_multiple.md)
    - [Controller power on](bluetooth/bluetooth_controller_power_on.md)
    - [Controller discoverable](bluetooth/bluetooth_controller_discoverable.md)
    - [Controller pairable](bluetooth/bluetooth_controller_pairable.md)
  - [Device](<>)
    - [Device scan](bluetooth/bluetooth_device_scan.md)
    - [Device list](bluetooth/bluetooth_device_list.md)
    - [Device pair](bluetooth/bluetooth_device_pair.md)
    - [Device connect](bluetooth/bluetooth_device_connect.md) [Usage](<>)
    - [Connection](bluetooth/bluetooth_connect.md)

- [Cron](cron/cron.md)

  - [Format of crontab](cron/cron_format.md)
  - [Usage](cron/cron_usage.md)
  - [List User Scheduled Command](cron/cron_list.md)
  - [Delete User Scheduled Command](cron/cron_delete.md)
  - [File for Each User](cron/cron_files.md)
  - [Permission of Scheduled Command](cron/cron_permission.md)

- [Dediprog](dediprog/dediprog.md)

  - [Linux Guide](dediprog/dediprog_guide.md)

- [Drivers](drivers/drv.md)

  - [USB](drivers/usb/usb.md)
    - [Power](drivers/usb/usb_pwr.md)

- [GPG](gpg/gpg.md)

  - [Generate Key](gpg/gpg_generate_keypair.md)
  - [List Key](gpg/gpg_list_keys.md)
  - [Export key](gpg/gpg_export_key.md)
  - [Import key](gpg/gpg_import_key.md)

- [Khal](khal/khal.md)

  - [Usage](khal/khal_usage.md)
  - [(atd) Calendar Notification](khal/khal_at.md)
  - [(vdirsyncer) Calendar remote sync](khal/khal_vdirsyncer.md)

- [LSP](lsp/lsp.md)

  - [Python](lsp/python.md)
    - [Installation](lsp/python_install.md)
    - [Ruff](lsp/python_ruff_usage.md)

- [Pandoc](./pandoc/pandoc.md)

  - [Installation](pandoc/pandoc_install.md)
  - [Usage](./pandoc/pandoc_usage.md)

- [Pass](pass/pass.md)

  - [Installation](pass/pass_installation.md)
  - [Create Password](pass/pass_create.md)
  - [List Password](pass/pass_list.md)
  - [Search Password](pass/pass_search.md)
  - [Show Password](pass/pass_show.md)
  - [Change Password](pass/pass_change.md)
  - [Delete Password](pass/pass_delete.md)
  - [Restore Password](pass/pass_restore.md)
  - [Add info in Password](pass/pass_add_info.md)

- [SSH](ssh/ssh.md)

  - [Key generator](ssh/ssh_gen.md)
  - [Usage](ssh/ssh_usage.md)
  - [Disown](ssh/ssh_disown.md)
  - [Nohup](ssh/ssh_nohup.md)
  - [TFTP](ssh/tftp.md)
  - [SFTP](ssh/sftp.md)

- [Saleae](saleae/saleae.md)

  - [Note](saleae/saleae_note.md)

- [RSync](rsync/rsync.md)

  - [Transfer](rsync/rsync_transfer.md)

- [Sensors](./sensors/sensors.md)

  - [Display](./sensors/sensors_display.md)

- [OpenSSL](<>)

  - [MD5](openssl/md5.md)

- [Mount](mount/mount.md)

  - [USB Format](mount/mount_usb_format.md)
  - [WSL - Mount USB from Window](mount/mount_wsl_usb_dev.md)
  - [WSL - USB Mass Storage Device](mount/mount_wsl_usb_mass_storage_dev.md)
  - [Format USB drive](mount/mount_format_usb_drive.md)
  - [Create Bootable USB drive](mount/mount_create_bootable_usb.md)
  - [Virtual Machine - Link Host Directory](mount/mount_vm_host_dir.md)

- [Wifi](wifi/home.md)

- [Xxd](xxd/xxd.md)

  - [Usage](xxd/xxd_usage.md)

- [Other](other/README.md)

  - [PiCar-X](other/picar_x/README.md)
    - [Intsallation](other/picar_x/installation.md)
    - [Control Servo](other/picar_x/servo_control.md)

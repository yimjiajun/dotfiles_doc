# cron

**Execute scheduled commands**

`cron` is daemon to execute scheduled commands

`crontab` is maintain *crontab* files for individual users

- crontab  is the program used to install, deinstall or list the tables used to drive the cron(8) daemon
  in  Vixie  Cron. <br>
  Each  user  can  have  their  own  crontab,  and  though   these   are   files   in
  */var/spool/cron/crontabs*, they are not intended to be edited directly.

|   | Sections                         | sub-Sections                                          |
| - | -                                | -                                                     |
| 1 | [Format](cron_format.md)         |                                                       |
| 2 | [Usage](cron_usage.md)           | [editor](cron_usage.md#crontab-x-default-text-editor) |
|   |                                  | [file](cron_usage.md#crontab-x-file)                  |
| 3 | [Lists](cron_list.md)            |                                                       |
| 4 | [Delete](cron_delete.md)            |                                                       |
| 5 | [Files](cron_files.md)           |                                                       |
| 6 | [Permission](cron_permission.md) |                                                       |

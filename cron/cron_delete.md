# cron - Delete

**Delete user scheduled command**

```bash
crontab -r $USER
```

# cron - Permissions

**Limit the scheduled command to run**

```
/etc/cron.allow
/etc/cron.deny
```

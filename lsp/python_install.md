# Installation

Using `pip`:

1. Install **[Ruff](https://github.com/astral-sh/ruff)**:

    ```bash
    pip install ruff
    ```

# LSP (Language Server Protocol)

The Language Server Protocol (LSP) is a common protocol for communication between editors and language servers.
 The goal of the Language Server Protocol (LSP) is to standardize the protocol for how tools and servers communicate.
 This allows for a single language server (e.g. PHP, Python, etc.) to be re-used in multiple development tools,
 which in turn allows for more robust language support for editors.

![LSP](https://miro.medium.com/v2/resize:fit:1358/1*NWvQepJvLQJLZLkLbNnEzA.png)

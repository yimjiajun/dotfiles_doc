# RSYNC

`rsync` is a file transfer program capable of efficient remote update
 via a fast differencing algorithm.

Sync new updated/modified sources on target device,
 which minimum  the transfer size of data to target device and more fastest.

1. [Transfer](rsync_transfer.md)

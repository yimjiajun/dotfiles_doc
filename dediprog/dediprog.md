# Dediprog

Founded in 2005 in Taiwan, [DediProg](https://www.dediprog.com/category/spi-nor-flash-device-programmer) has been the leading brand in providing IC programming devices and IC test sockets. They combine the efficiency of programming hardware with infrastructure software from a diversity of industry leaders to deliver a total IC programming solution for customers.

![dediprog_sf600.png](https://old-www.dediprog.com/upload_file/download/126.png)

- [Guide](dediprog/dediprog_guide.md)

# Other

1. [PiCar-X](picar_x/README.md)


    > The PiCar-X is an AI-driven self-driving robot car for the Raspberry Pi platform,
    > upon which the Raspberry Pi acts as the control center.

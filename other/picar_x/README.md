# PiCar-X

- [Topic](#topic)

- [Introduction](#introduction)

## Topic

1. [Installation](installation.md)

2. [Control Servo](servo_control.md)

## Introduction

The PiCar-X is an AI-driven self-driving robot car for the Raspberry Pi platform,
upon which the Raspberry Pi acts as the control center.

The PiCar-X’s 2-axis camera module, ultrasonic module,
and line tracking modules can provide the functions of color/face/traffic-signs detection,
automatic obstacle avoidance, automatic line tracking, etc.

![PiCar-X](https://docs.sunfounder.com/projects/picar-x/en/latest/_images/picar-x.jpg)

- [PiCar-X documentations](https://docs.sunfounder.com/projects/picar-x/en/latest/)

- [Quick guide of Python](https://docs.sunfounder.com/projects/picar-x/en/latest/python/python_start/quick_guide_on_python.html)

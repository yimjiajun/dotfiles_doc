# Bluetooth - Lists scanned / discovered devices

List all been scan / discovered Bluetooth device
 by [scan](/doc/bluetooth/bluetooth_device_scan.md)

```bash
bluetoothctl devices
```

Output response: *available Bluetooth devices*

```plain
...
Device 10:2B:41:17:19:7D [TV] Samsung TU7000 55 TV
...

Device F8:4E:17:A7:72:D1 WH-XB910N
...
```

# Bluetooth - Pairable Controller

Turn on controller to pairs,
 when controller is not pairable ( [Pairable: no](bluetooth_controller_status.md) )

```bash
bluetoothctl pairable on
```

Output response: *pairable*

```plain
Changing pairable on succeeded
```

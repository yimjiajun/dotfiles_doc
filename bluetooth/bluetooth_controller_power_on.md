# Bluetooth - Controller Power On

Turn on Bluetooth controller,
 when powered is off ( [Powered: no](bluetooth_controller_status.md) )

```bash
bluetoothctl power on
```

Output response: *power on*

```plain
Changing power on succeeded
```

# Bluetooth - Discoverable Controller

Start discover by Bluetooth controller,
 when Discoverable is off ([Discoverable: no](bluetooth_controller_status.md))

```bash
bluetoothctl discoverable on
```

Output response: *Discoverable: yes*

```plain
Changing discoverable on succeeded
```

# Bluetooth - Interactive

Interactive Bluetooth control tools

```bash
bluetoothctl
```

Output response: *Interactive shell*

```plain
Agent registered
[bluetooth]#
```

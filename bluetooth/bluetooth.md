# Bluetooth

Connect to a Bluetooth device via the terminal.

This process involves configuring our Bluetooth controller,
 pairing it to our target device.

reference: [Connect to Bluetooth Device
 via the Terminal](https://www.baeldung.com/linux/bluetooth-via-terminal)

- [Interactive](bluetooth_interactive.md)
- Controller
  - [Controller status](bluetooth_controller_status.md)
  - [Controller multiple](bluetooth_controller_multiple.md)
  - [Controller power on](bluetooth_controller_power_on.md)
  - [Controller discoverable](bluetooth_controller_discoverable.md)
  - [Controller pairable](bluetooth_controller_pairable.md)
- Device
  - [Device scan](bluetooth_device_scan.md)
  - [Device list](bluetooth_device_list.md)
  - [Device pair](bluetooth_device_pair.md)
  - [Device connect](bluetooth_device_connect.md)
- Usage
  - [Connection](bluetooth_connect.md)

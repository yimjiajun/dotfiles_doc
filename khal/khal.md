# Khal

## Introduction

[Khal](https://github.com/pimutils/khal) is a standards based CLI and terminal calendar program, able to synchronize with [CalDAV](https://en.wikipedia.org/wiki/CalDAV) servers through [vdirsyncer](https://github.com/pimutils/vdirsyncer).

- [Usage](khal/khal_usage.md)
- [Calendar notification](khal/khal_at.md)
- [Calendar remote synchronize](khal/khal_vdirsyncer.md)
